/*******************************************************************************
 * Copyright ©2002-2019 Skava - All rights reserved.
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.repository;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.skava.yl.feed.report.model.entity.ReportingPreferenceEntity;

/**
 * This is an interface to be implemented for combined DB operations within
 * subscription feed
 * 
 * @author lakshmi.s41
 *
 */
@Repository
public interface ReportingPreferenceRepository extends CrudRepository<ReportingPreferenceEntity, Long>,
  QuerydslPredicateExecutor<ReportingPreferenceEntity> {
}
