/*******************************************************************************
 * Copyright ©2002-2019 Skava - All rights reserved.
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import com.querydsl.core.annotations.Config;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@NoArgsConstructor
@Config(defaultVariableName = ReportingFeedConstants.USERSUBSCRIPTION_DEFAULT_VARIABLE_NAME)
@Entity(name = ReportingFeedConstants.USERSUBSCRIPTION_TABLE_NAME)
@Table(name = ReportingFeedConstants.USERSUBSCRIPTION_TABLE_NAME)
public class ReportingPreferenceEntity implements Serializable {
  /**
   * serialVersionUID
   */
  private static final long serialVersionUID = -952273639811811332L;

  /**
   * It refers to the identifier of subscription
   */
  @Getter
  @Setter
  @Id
  @Column(name = ReportingFeedConstants.USER_ID_NAME)
  private long userId;

  /** The collection id. */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.COLLECTION_ID_NAME)
  private long collectionId;

  /**
   * It refers to the order freeze processing date.
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.FREEZE_ORDER_PROCESSING_DATE)
  private int freezeOrderDate;

  /**
   * It refers to the order notification processing date.
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.NOTIFICATION_ORDER_PROCESSING_DATE)
  private int notificationOrderDate;

  /**
   * It refers to the order recursive processing date.
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.RECURRENCE_ORDER_PROCESSING_DATE)
  private int orderProcessingDate;

  /**
   * It refers the identifier of the shipping
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.SHIPPING_ID, length = 100)
  private String shippingId;

  /**
   * It refers the type of the shipping
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.SHIPPING_TYPE, length = 100)
  private String shippingType;

  /**
   * It refers shipping method
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.SHIPPING_METHOD, length = 100)
  private String shippingMethod;

  /**
   * It refers shipping instruction
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.SHIPPING_INSTRUCTION, length = 100)
  private String shippingInstruction;

  /**
   * It refers shipping delivery period
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.DELIVERY_PEREIOD)
  private long deliveryPeriod;

  /**
   * It refers payment detail.
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.PAYMENT_ID)
  private String paymentId;

  /**
   * It refers address detail.
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.ADDRESS_ID)
  private String addressId;

  /**
   * It refers last order processing date.
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.LAST_ORDER_PROCESSING_DATE)
  private Date lastOrderProcessingDate;

  /**
   * It refers process now flag.
   */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.PROCESS_NOW)
  private boolean processNow;

  /** Created time */
  @Getter
  @Setter
  @CreationTimestamp
  @Column(name = ReportingFeedConstants.CREATED_TIME)
  public Date createdTime;

  /** Updated time */
  @Getter
  @UpdateTimestamp
  @Column(name = ReportingFeedConstants.UPDATED_TIME)
  public Date updatedTime;

  /** Identifier of the created user */
  @Getter
  @Setter
  @CreatedBy
  @Column(name = ReportingFeedConstants.CREATED_BY, length = 100)
  public String createdBy;

  /** Identifier of the updated user */
  @Getter
  @Setter
  @LastModifiedBy
  @Column(name = ReportingFeedConstants.UPDATED_BY, length = 100)
  public String updatedBy;

  /** It refers last order status. */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.LAST_ORDER_STATUS)
  private String lastOrderStatus;

  /** It refers last order status message. */
  @Getter
  @Setter
  @Column(name = ReportingFeedConstants.LAST_ORDER_STATUS_MESSAGE)
  private String lastOrderStatusMessage;

}
