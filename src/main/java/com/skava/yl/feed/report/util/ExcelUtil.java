/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used to create and write the Excel file
 * @author sowbaranika.t
 *
 */

public final class ExcelUtil {
  private static final Logger LOGGER = LoggerFactory.getLogger(ExcelUtil.class);

  private ExcelUtil() {
    super();
  }

  /**
   * Methhod used to create the Excel file in the specified location
   * @param directory Holds the directory name
   * @param fileName Holds the file name to be created
   * @return returns the file created
   */
  private static File createExcel(String directory, String fileName) {
    File excelFile = new File(fileName);
    try (FileOutputStream outputStream = new FileOutputStream(excelFile)) {
      if (!excelFile.exists()) {
        File directoryFile = new File(directory);
        excelFile = File.createTempFile(fileName, ".xlsx", directoryFile);
      }
      XSSFWorkbook workBook = new XSSFWorkbook();
      workBook.write(outputStream);
    } catch (FileNotFoundException e) {
      LOGGER.error("File Not found {}", e);
    } catch (IOException e) {
      LOGGER.error("Error in Creating the xlsx file {}", e);
    }
    return excelFile;
  }

  /**
   * Method used o write the content to the file
   * @param directory Holds the directiry name
   * @param fileName Holds the filename
   * @param rowData data to be written on to the file
   * @return returns the updated file
   */
  public static File writeExcel(String directory, String fileName, Object rowData) {

    HashMap<String, List<Long>> data = (HashMap<String, List<Long>>) rowData;
    List<String> column = new ArrayList<>();
    Iterator iter = data.entrySet().iterator();
    File excelFile = new File(directory + fileName);
    while (iter.hasNext()) {
      List<String> sheetName = new ArrayList<>();
      Map.Entry<String, List<Long>> entry = (Entry<String, List<Long>>) iter.next();
      sheetName.add(entry.getKey());
      column.add(entry.getKey());
      try {
        if (!excelFile.exists()) {
          excelFile = createExcel(directory, fileName);
        }
        FileInputStream inputStream = new FileInputStream(excelFile);
        XSSFWorkbook workbo = new XSSFWorkbook(inputStream);
        processSheetData(workbo, sheetName, entry.getValue(), column.size());
        inputStream.close();
        FileOutputStream outputStream = new FileOutputStream(excelFile);
        workbo.write(outputStream);
        outputStream.close();
      } catch (FileNotFoundException e) {
        LOGGER.error("File Not found {}", e);
      } catch (IOException e) {
        LOGGER.error("Error in Writing the xlsx file {}", e);
      }
    }
    return excelFile;
  }

  /**
   * Method used to get the sheet details and create new sheets
   * @param workbo workbook in which sheet has to created
   * @param sheetName name to the Sheet
   * @param entry Holds the Data to be written
   * @param size holds the coulum size.
   */
  public static void processSheetData(XSSFWorkbook workbo, List<String> sheetName, List<Long> entry,
    int size) {
    for (String sheetNam : sheetName) {
      Sheet sheet = workbo.getSheet(sheetNam);
      if (sheet == null) {
        sheet = workbo.createSheet(sheetNam);
      }
      if (entry != null && !entry.isEmpty()) {
        processRowData(sheet, entry, size);
      }
    }
  }

  /**
   * Method used to write the row data to the sheet
   * @param sheet Sheet object in which data has to be written
   * @param entry data to be wrritten to the sheet
   * @param size column size
   */
  public static void processRowData(Sheet sheet, List<Long> entry, int size) {
    for (int rowidx = 0; rowidx < entry.size(); rowidx++) {
      for (int columnidx = 0; columnidx < size; columnidx++) {
        Row successrow = sheet.createRow(rowidx);
        Cell cell = successrow.createCell(columnidx);
        cell.setCellValue(entry.get(rowidx));
      }
    }
  }
}
