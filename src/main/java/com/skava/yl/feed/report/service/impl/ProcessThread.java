/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;

import com.querydsl.core.types.Predicate;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;
import com.skava.yl.feed.report.helper.ReportingFeedUtil;
import com.skava.yl.feed.report.process.ReportingFeedProcess;
import com.skava.yl.feed.report.repository.ReportingPreferenceRepository;

/**
 * This class is to process multiple preferences in a individual thread
 * @author sowbaranika.t
 */

public class ProcessThread {
  private static final Logger LOGGER = LoggerFactory.getLogger(ProcessThread.class);
  @Autowired
  private ReportingPreferenceRepository subscriptionPreferenceRepository;
  /**
   * used to construct the query using predicate.
   */
  private Predicate predicate;
  /**
   * Holds the total page Count details
   */
  private int totalPageCount;
  /**
   * Holds the total record limits that is to be processed at a time
   */
  private int limit;
  /**
   * object used to access the subscriptionFeedUtill methods and variables.
   */
  private ReportingFeedUtil subscriptionFeedUtil;

  /**
   * Constructor to initailize all the required values for the class
   * 
   * @param predicate object to construct query
   * @param totalPageCount holds total page count
   * @param limit holds the limit of records to be processed
   * @param subscriptionPreferenceRepository holds the repository methods
   * @param subscriptionFeedUtil can make use of all the methods and variables
   */
  public ProcessThread(Predicate predicate, int totalPageCount, int limit,
    ReportingPreferenceRepository subscriptionPreferenceRepository,
    ReportingFeedUtil subscriptionFeedUtil) {
    this.predicate = predicate;
    this.totalPageCount = totalPageCount;
    this.limit = limit;
    this.subscriptionPreferenceRepository = subscriptionPreferenceRepository;
    this.subscriptionFeedUtil = subscriptionFeedUtil;
  }

  /**
   * Method that executes asynchronously, upon forking a new thread per subscription preference
   */
  @Async("taskExecutor")
  public void processSubscriptionPreference() {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "Thread run",
      ReportingFeedConstants.METHOD_ENTERED);
    Long startTime = System.currentTimeMillis();
    LOGGER.info("Background Feed Processing Started at StartTime: {}", startTime);
    for (int pageItr = 0; pageItr < totalPageCount; pageItr++) {
      Pageable pageable = PageRequest.of(pageItr, limit);
      LOGGER.info("Current Page: {}", pageable);
      ReportingFeedProcess subscriptionFeedProcess = new ReportingFeedProcess(predicate, pageable,
        subscriptionPreferenceRepository, subscriptionFeedUtil);
      subscriptionFeedProcess.processSubscriptionPreference();
    }
    Long endTime = System.currentTimeMillis() - startTime;
    LOGGER.info("Background Feed Processing ended and TimeTaken(in MilliSeconds): {}", endTime);
    LOGGER.debug("Completed Threads List:{}", Thread.activeCount());

    if (subscriptionFeedUtil.feedConfig.getNotificationConfig() != null) {
      subscriptionFeedUtil.sendNotification();
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "Thread run",
      ReportingFeedConstants.METHOD_EXITED);
  }
}
