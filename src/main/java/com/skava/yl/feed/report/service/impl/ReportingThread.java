/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.service.impl;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

import com.querydsl.core.types.Predicate;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;
import com.skava.yl.feed.report.helper.ReportingFeedUtil;
import com.skava.yl.feed.report.helper.Utils;
import com.skava.yl.feed.report.model.entity.ReportingPreferenceEntity;
import com.skava.yl.feed.report.repository.ReportingPreferenceRepository;

/**
 * Class used to process the Subscription data from the DB and make the processNow call.
 * @author lakshmi.s41
 *
 */
class ReportingThread extends Thread {
  private static final Logger LOGGER = LoggerFactory.getLogger(ReportingThread.class);
  /**
   * used to construct the query using predicate.
   */
  private Predicate predicate;
  /**
   * Holds the pagable data with different offset and limit.
   */
  private Pageable pageable;
  /**
   * Holds the methods of repository class
   */
  private ReportingPreferenceRepository subscriptionPreferenceRepository;
  /**
   * object used to access the subscriptionFeedUtill methods and variables.
   */
  private ReportingFeedUtil subscriptionFeedUtil;

  /**
   * Constructor used to initialize the variables 
   * 
   * @param predicate
   * @param pageable
   * @param subscriptionPreferenceRepository
   * @param subscriptionFeedUtil
   */
  public ReportingThread(Predicate predicate, Pageable pageable,
    ReportingPreferenceRepository subscriptionPreferenceRepository,
    ReportingFeedUtil subscriptionFeedUtil) {
    this.predicate = predicate;
    this.pageable = pageable;
    this.subscriptionPreferenceRepository = subscriptionPreferenceRepository;
    this.subscriptionFeedUtil = subscriptionFeedUtil;
  }

  /**
   * Method to find all preferences and process each
   */
  @Override
  public void run() {
    Page<ReportingPreferenceEntity> preferences = subscriptionPreferenceRepository.findAll(predicate, pageable);

    List<ReportingPreferenceEntity> response = preferences.getContent();
    for (ReportingPreferenceEntity preference : response) {
      String processNowurl = subscriptionFeedUtil.processNowAPIURl(preference.getUserId());
      String session = subscriptionFeedUtil.getSession(false, String.valueOf(preference.getUserId()));
      HashMap<String, String> headersMap = new HashMap<>();
      headersMap.put(ReportingFeedConstants.PARAM_X_SESSION_ID, session);
      headersMap.put("isUserSubscriptionPreferenceDemo", "true");
      try {
        ResponseEntity<String> processNowAPIResponse = Utils.makeAPIRequest(null, processNowurl, String.class,
          headersMap, HttpMethod.POST);
        LOGGER.error("Error in processing Subscription {}", processNowAPIResponse.getBody());

      } catch (HttpStatusCodeException e) {
        LOGGER.error("Exception in processing the Subscription thread details {}", e);
      }
    }
  }
}
