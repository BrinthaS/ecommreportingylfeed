/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.helper;

import java.time.ZonedDateTime;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * This file handles all the http methods that are used in our project
 */
public final class Utils {

  private Utils() {
    super();
  }

  /**
   * 
   * @param <T> Generic type Model object iis allowed
   * @param requestObj Holds the Postbody data for the URL request
   * @param url Holds URl string
   * @param responseType Holds the responseType class name
   * @param headers Holds the header details
   * @param method Holds the URl Http method
   * @return Returns the URLS response of its reponse model.
   */
  public static <T> ResponseEntity<T> makeAPIRequest(Object requestObj, String url, Class<T> responseType,
    Map<String, String> headers, HttpMethod method) {

    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    RestTemplate restTemplate = new RestTemplate(requestFactory);
    HttpEntity<Object> httpEntity = new HttpEntity<>(requestObj, Utils.setHeaders(headers));
    return restTemplate.exchange(url, method, httpEntity, responseType);
  }

  public static HttpHeaders setHeaders(Map<String, String> headers) {
    HttpHeaders header = new HttpHeaders();
    if (headers != null) {
      for (Map.Entry<String, String> entry : headers.entrySet()) {
        header.set(entry.getKey(), entry.getValue());
      }
    }
    return header;
  }

  /**
   * Method to gets the URL from config
   * 
   * @param key Which refers the serviceName for which the URl has to be consider
   * @param serviceConfig Which holds the request Object by the enduser with all service's Url details.
   * @return returns the specific services URL from Config given.
   */
  public static String getUrl(String key, JSONObject serviceConfig) {
    JSONObject config = serviceConfig.getJSONObject(key);
    return config.getString("endPoint") + "/" + config.getString("name");
  }

  /**
   * Get's the start of the day.
   * 
   * @param curDate the current date.
   * @return the time milliseconds of the time.
   */
  public static long startOfDay(ZonedDateTime curDate) {
    return curDate.toLocalDate().atStartOfDay().atZone(curDate.getZone()).withEarlierOffsetAtOverlap().toInstant()
      .toEpochMilli();
  }

  /**
   * Get's the end of the day
   * 
   * @param curDate the current date.
   * @return the time milliseconds of the time.
   */
  public static long endOfDay(ZonedDateTime curDate) {
    ZonedDateTime startOfTomorrow = curDate.toLocalDate().plusDays(1).atStartOfDay().atZone(curDate.getZone())
      .withEarlierOffsetAtOverlap();
    return startOfTomorrow.minusSeconds(1).toInstant().toEpochMilli();
  }

}
