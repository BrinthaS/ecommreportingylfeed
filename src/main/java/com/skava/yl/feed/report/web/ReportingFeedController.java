/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 *This is the controller class for subscription feed service
 */
package com.skava.yl.feed.report.web;

import org.audit4j.core.annotation.Audit;
import org.audit4j.core.annotation.AuditField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.skava.core.constant.DocumentationConstants;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;
import com.skava.yl.feed.report.model.request.ReportingScheduleRequest;
import com.skava.yl.feed.report.model.request.ReportingScheduleRequestInternal;
import com.skava.yl.feed.report.model.responce.ReportingFeedServiceErrorResponse;
import com.skava.yl.feed.report.service.ReportingFeeedService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import lombok.NoArgsConstructor;

/**
 * Controller Class to trigger the Feeds
 * @author sowbaranika.t
 *
 */
@SwaggerDefinition(info = @Info(title = "Subscription Feed", version = "v8.0", description = "SubscriptionFeed"))
@Api(value = "SubscriptionFeed", tags = "Subscription Feed")
@RestController
@RequestMapping("/feeds")
@NoArgsConstructor
public class ReportingFeedController {

  /**
   * Holds all the methods in the object
   */
  @Autowired
  private ReportingFeeedService reportingFeeedService;

  /**
   * Method used to trigger the subscription Feed
   * @param authToken Token to authenticate the user
   * @param versionId Holds the projects versions
   * @param storeId Holds the StoreId for which the feed has to be triggered
   * @param request Hold all the user required params that are used to trigger the feed
   * @return Returns the success response
   */
  @ApiOperation(value = "", notes = "", nickname = "triggerSubscriptionFeed")
  @ResponseStatus(HttpStatus.CREATED)
  @ApiResponses(value = {@ApiResponse(code = 201, message = "created", response = String.class),
    @ApiResponse(code = 400, message = "Bad request", response = ReportingFeedServiceErrorResponse.class),
    @ApiResponse(code = 401, message = "Unauthorized", response = ReportingFeedServiceErrorResponse.class),
    @ApiResponse(code = 403, message = "Forbidden", response = ReportingFeedServiceErrorResponse.class),
    @ApiResponse(code = 404, message = "Not found", response = ReportingFeedServiceErrorResponse.class),
    @ApiResponse(code = 409, message = "Conflict", response = ReportingFeedServiceErrorResponse.class),
    @ApiResponse(code = 422, message = "Unprocessable Entity", response = ReportingFeedServiceErrorResponse.class),
    @ApiResponse(code = 500, message = "Internal Server error", response = ReportingFeedServiceErrorResponse.class)})
  @PostMapping
  @Audit
  public ResponseEntity<String> triggerSubscriptionFeed(
    @ApiParam(value = DocumentationConstants.AUTH_TOKEN_DESC, required = true) @RequestHeader(value = "x-auth-token",
      required = true) String authToken,
    @ApiParam(value = "${SubscriptionSwagger.common.version.ApiParam.value}",
      defaultValue = ReportingFeedConstants.CONST_DEFAULT_API_VERSION_ID, required = false,
      example = ReportingFeedConstants.CONST_DEFAULT_API_VERSION_ID,
      allowEmptyValue = true) @RequestHeader(value = "x-version-id", required = false) String versionId,
    @ApiParam(value = "Holds a valid unique identifier of the store.", required = false) @RequestHeader(
      value = "x-store-id", required = true) @AuditField(field = "STORE_ID") long storeId,
    @ApiParam(value = "${SubscriptionSwagger.common.request.ApiParam.value}",
      required = true) @RequestBody(required = true) ReportingScheduleRequest request) {
    ReportingScheduleRequestInternal feedRequest = new ReportingScheduleRequestInternal();
    feedRequest.setRequest(request);
    feedRequest.setStoreId(storeId);
    String reponse = reportingFeeedService.process(feedRequest);

    return new ResponseEntity<>(reponse, HttpStatus.OK);
  }

}
