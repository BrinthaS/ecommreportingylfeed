/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.stereotype.Component;

import com.skava.core.ECommerceException;
import com.skava.core.auth.CustomSecurityConfigurer;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;

import lombok.NoArgsConstructor;

/**
 * PriceSecurityConfigurer is a implementation of CustomSecurityConfigurer
 * Filter to exclude authorize needs to be specified in this class Retrieving
 * the business Id from the collection Id needs to be specified in this class
 */
@NoArgsConstructor
@Component
public class ReportingSecurityConfigurer implements CustomSecurityConfigurer {

  /** The Price Security Configurer logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(ReportingSecurityConfigurer.class);

  /*
   * (non-Javadoc)
   * 
   * @see com.skava.core.auth.CustomSecurityConfigurer
   * customConfigure(org.springframework.security.config.annotation.web.builders.
   * HttpSecurity) Path to skip the authorization will be configured in this
   * method.
   */
  @Override
  public void customConfigure(HttpSecurity httpSecurity) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "CustomConfigure",
      ReportingFeedConstants.METHOD_ENTERED);
    try {
      httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    } catch (Exception exception) {
      LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "customConfigure",
        ReportingFeedConstants.METHOD_EXCEPTION);
      throw new ECommerceException("authorization.configuration.failed", exception.getMessage(), exception);
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "customConfigure",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.skava.core.auth.CustomSecurityConfigurer#getBusinessId(java.lang.Long)
   * Logic to retrieve the business id from the collection id is implemented here.
   */
  @Override
  public Long getBusinessId(Long id) {
    return 1L;
  }
}
