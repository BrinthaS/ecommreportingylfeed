/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 * This file contains constants needed for subscription feed
 */
package com.skava.yl.feed.report.constant;

/**
 * @author brintha.s01
 *
 */
public final class ReportingFeedConstants {
  /**
   * SUBSCRIPTION INTRODUCTION - SWAGGER DOCUMENTATION
   **/
  public static final String SUBSCRIPTION_INTRO = "<div><p><b>Subscription Feed</b> is the strategic "
    + "to trigger subscription feeds</p></div>";
  /*
   * API Version Id
   */
  public static final String CONST_DEFAULT_API_VERSION_ID = "8.6.2";
  // Subscription Preference Entity Name
  public static final String USERSUBSCRIPTION_TABLE_NAME = "subscriptionpreference";
  // Subscription Preference default variable name
  public static final String USERSUBSCRIPTION_DEFAULT_VARIABLE_NAME = "subscriptionpreference";

  public static final String USER_ID_NAME = "userid";
  public static final String COLLECTION_ID_NAME = "collectionid";
  public static final String RECURRENCE_ORDER_PROCESSING_DATE = "order_processing_date";
  public static final String FREEZE_ORDER_PROCESSING_DATE = "freeze_order_date";
  public static final String NOTIFICATION_ORDER_PROCESSING_DATE = "notification_order_date";
  public static final String SHIPPING_ID = "shipping_id";
  public static final String SHIPPING_TYPE = "shipping_type";
  public static final String SHIPPING_METHOD = "shipping_method";
  public static final String SHIPPING_INSTRUCTION = "shipping_instruction";
  public static final String DELIVERY_PEREIOD = "deliveryperiod";
  public static final String PAYMENT_ID = "payment_id";
  public static final String ADDRESS_ID = "address_id";
  public static final String LAST_ORDER_PROCESSING_DATE = "last_order_processing_date";
  public static final String PROCESS_NOW = "process_now";
  public static final String LAST_ORDER_STATUS = "last_order_status";
  public static final String LAST_ORDER_STATUS_MESSAGE = "last_order_status_message";

  /**
   * Field name to hold Created Time of the subscription
   */
  public static final String CREATED_TIME = "created_time";

  /**
   * Field name to hold Updated Time of the subscription
   */
  public static final String UPDATED_TIME = "updated_time";

  /**
   * Field name to hold Identifier of the created user
   */
  public static final String CREATED_BY = "created_by";

  /**
   * Field name to hold Identifier of the updated user
   */
  public static final String UPDATED_BY = "updated_by";
  /**
   * Field name to hold feedRequest details from the user.
   */
  public static final String SERVICE_CONFIG = "serviceConfig";
  public static final String NOTFICATION_SERVICE_CONFIG = "notificationConfig";
  public static final String AUTH_CONFIG = "authConfig";
  public static final String SERVICE_CONFIG_ORCH_CONSTANT = "orchestration";
  public static final String SERVICE_CONFIG_NOTIFY_CONSTANT = "notification";
  public static final String SERVICE_CONFIG_AUTH_CONSTANT = "auth";
  public static final String SERVICE_CONFIG_FOUNDATION_CONSTANT = "foundation";
  public static final String SERVICE_CONFIG_SUBSCRIPTION_CONSTANT = "subscription";
  public static final String SERVICE_CONFIG_CUSTOMER_CONSTANT = "customer";
  public static final String THREAD_CONFIG = "threadLimit";

  public static final String AUTH_CONFIG_USER_NAME = "userName";
  public static final String DEFAULT_AUTH_COLLECTION_ID = "1";

  public static final String USER_AUTH_URL = "/users/auth";
  public static final String STORE_URL = "/stores/";
  public static final String AUTH_URL = "/sessions?userid=";

  public static final String SUBSCRIPTION_PREFERENCE_SUFFIX_URL = "/storefront/subscriptions/preferences/";
  public static final String PARAM_STORE_ID = "storeId";
  public static final String PARAM_QUESTION = "?";
  public static final String PARAM_EQUAL = "=";
  public static final String PARAM_AMP = "&";

  public static final String APPLY_STORE_CREDIT = "isApplyStoreCredit";
  public static final String X_SESSIONID_PREFIX = "x-sk-session-id";

  public static final String PARAM_X_COLLECTION_ID = "x-collection-id";
  public static final String PARAM_X_API_KEY = "x-api-key";
  public static final String PARAM_X_AUTH_TOKEN = "x-auth-token";
  public static final String PARAM_AUTH_TOKEN = "authToken";
  public static final String PARAM_X_API_KEY_VALUE = "testApiKey";

  public static final String FIELD_COLLECTIONID = "collectionId";
  public static final String FIELD_NAME = "name";
  public static final String FIELD_TIME_ZONE = "timeZone";
  public static final String FIELD_ASSOCIATIONS = "associations";

  public static final String PARAM_USER_IDENTITY = "identity";
  public static final String PARAM_X_AUTHTOKEN = "x-auth-token";
  public static final String PARAM_X_SESSION_ID = "x-sk-session-id";

  public static final String FIELD_RECIPENTS_EMAIL = "recipentsEmail";
  public static final String FIELD_TEMPLATE_NAME = "templateName";
  public static final String FIELD_ATTACHMENTS_NAME = "attachmentsName";

  public static final String NOTIFICATION_REQUEST_NAME = "name";
  public static final String NOTIFICATION_REQUEST_RECIPIENTS = "recipients";
  public static final String NOTIFICATION_REQUEST_EMAIL = "email";
  public static final String NOTIFICATION_REQUEST_MACROS = "macros";
  public static final String NOTIFICATION_REQUEST_ATTACHEMENTS = "attachments";
  public static final String NOTIFICATION_REQUEST_ATTACHEMENT_FILE_NAME = "fileName";
  public static final String NOTIFICATION_REQUEST_ATTACHEMENT_FILE_CONTENT = "content";
  public static final String NOTIFICATION_REQUEST_ATTACHEMENT_CONTENT_SOURCE = "contentSource";

  public static final String RETRY_COUNT_VALUE = "retryCount";
  public static final String ENABLE_DEMO_MODE_VALUE = "enableDemoMode";
  public static final String PARAM_LIMIT = "limit";
  public static final String PARAM_CURRENT_DAY = "currentDay";
  public static final String PARAM_USER_ID = "userId";
  public static final String HEADER_IS_USER_SUBSCRIPTION_PREFERENCE_DEMO = "isUserSubscriptionPreferenceDemo";

  public static final long DEFAULT_RETRY_COUNT = 1;

  public static final String NOTIFICATION_URL = "/sendnotification";

  public static final int DEFAULT_LIMIT = 1000;
  /**
   * Debug Message format
   */
  public static final String DEBUG_MESSAGE_FORMAT = "### {} {} ###";
  /**
   * Constant for Method entered
   */
  public static final String METHOD_ENTERED = "Entered";

  /**
   * Constant for Method entered
   */
  public static final String METHOD_EXITED = "Exit";
  /**
   * Constant for Method Exception
   */
  public static final String METHOD_EXCEPTION = "Exception";

  public static final String CONST_METHOD_PROCESS = "process";

  public static final int RESPONSE_CODE_201 = 201;
  public static final int RESPONSE_CODE_404 = 404;
  public static final int RESPONSE_CODE_422 = 422;
  public static final int RESPONSE_CODE_500 = 500;
  public static final int RESPONSE_CODE_200 = 200;
  public static final int RESPONSE_CODE_400 = 400;
  public static final int RESPONSE_CODE_401 = 401;
  public static final int RESPONSE_CODE_403 = 403;
  public static final int RESPONSE_CODE_204 = 204;

  public static final String RESPONSE_MESSAGE_201 = "Created.";
  public static final String RESPONSE_MESSAGE_401 = "Unauthorized.";
  public static final String RESPONSE_MESSAGE_403 = "Forbidden.";
  public static final String RESPONSE_MESSAGE_404 = "Not found.";
  public static final String RESPONSE_MESSAGE_422 = "Unprocessed.";
  public static final String RESPONSE_MESSAGE_204 = "No Content";
  public static final String RESPONSE_MESSAGE_500 = "Internal Server error.";
  public static final String RESPONSE_MESSAGE_200 = "OK.";
  public static final String RESPONSE_MESSAGE_400 = "Bad Request";
  
  public static final String NOTIFICATION_FILE_NAME = "processNow";

  /** 
   * SubscriptionService Response Model 
   */
  public static final String SUBSCRIPTION_SERVICE_ERROR_RESPONSE_MODEL = "SubscriptionFeedServiceErrorResponse";

  private ReportingFeedConstants() {
    super();
  }

}
