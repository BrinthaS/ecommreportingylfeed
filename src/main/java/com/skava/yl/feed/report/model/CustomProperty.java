/**********************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof,
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *  
 * For further information contact Skava at info@skava.com.
 *********************************************************************************/
package com.skava.yl.feed.report.model;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomProperty implements Serializable {

  /**
   * This file contains custom properties for the subscription feed API To be
   * provided in the Request body
   */
  private static final long serialVersionUID = 5638950843654902606L;

  /**
   * Name of the param
   */
  @ApiModelProperty(value = "Name of the param")
  private String name;

  /**
   * String value for the param
   */
  @ApiModelProperty(value = "String value for the param")
  private String stringValue;
}
