/**
 * *******************************************************************************
 *
 *  Copyright ©2002-2018 Skava - All rights reserved.
 *
 *  All information contained herein is, and remains the property of Skava.
 *
 *  Skava including, without limitation, all software and other elements thereof,
 *
 *  are owned or controlled exclusively by Skava and protected by copyright, patent
 *
 *  and other laws. Use without permission is prohibited.
 *
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *
 *  For further information contact Skava at info@skava.com.
 *
 * *******************************************************************************
 */
package com.skava.yl.feed.report.model.request;

import java.util.List;

import com.skava.core.validation.InputModel;
import com.skava.yl.feed.report.model.CustomProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * This model holds the request data of a subscription schedule
 * @author lakshmi.s41
 *
 */
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(description = "This model holds the request data of a subscription schedule.")
public class ReportingScheduleRequest extends InputModel {

  private static final long serialVersionUID = -2411002090302534338L;
  /**
   * Params to be part of schedule request
   */
  @ApiModelProperty(value = "", required = true)
  private List<CustomProperty> params;

}
