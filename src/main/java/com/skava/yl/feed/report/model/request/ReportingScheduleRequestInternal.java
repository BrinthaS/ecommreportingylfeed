/**
 * *******************************************************************************
 *
 *  Copyright ©2002-2018 Skava - All rights reserved.
 *
 *  All information contained herein is, and remains the property of Skava.
 *
 *  Skava including, without limitation, all software and other elements thereof,
 *
 *  are owned or controlled exclusively by Skava and protected by copyright, patent
 *
 *  and other laws. Use without permission is prohibited.
 *
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *
 *  For further information contact Skava at info@skava.com.
 *
 * *******************************************************************************
 */
package com.skava.yl.feed.report.model.request;

import com.skava.core.validation.InputModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(description = "This model holds the request data of a subscription schedule.")
public class ReportingScheduleRequestInternal extends InputModel {

  /**
   * "This model holds the request data of a subscription schedule.
   */
  private static final long serialVersionUID = -2411002090302534338L;

  /**
   * API Request body
   */
  @ApiModelProperty(value = "", required = true)
  private ReportingScheduleRequest request;
 
  /**
   * StoreId for which the feed is scheduled
   */
  private long storeId;
}
