/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.service.impl;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;
import com.skava.yl.feed.report.helper.ReportingFeedUtil;
import com.skava.yl.feed.report.model.CustomProperty;
import com.skava.yl.feed.report.model.entity.QReportingPreferenceEntity;
import com.skava.yl.feed.report.model.request.ReportingScheduleRequestInternal;
import com.skava.yl.feed.report.repository.ReportingPreferenceRepository;
import com.skava.yl.feed.report.service.ReportingFeeedService;

/**
 * Class which holds the implementation part of all the service method
 * @author brintha.s01
 *
 */
@Service
public class ReportingFeedServiceImpl implements ReportingFeeedService {
  private static final Logger LOGGER = LoggerFactory.getLogger(ReportingFeedServiceImpl.class);
  /** Autowired the subscription Preference repository. */
  @Autowired
  private ReportingPreferenceRepository subscriptionPreferenceRepository;

  /**
   * object used to access the methods and variables
   */
  private ReportingFeedUtil subscriptionFeedUtil = new ReportingFeedUtil();

  /**
   * Constructor used to initailize the values
   * @param subscriptionPreferenceRepository
   */
  public ReportingFeedServiceImpl(ReportingPreferenceRepository subscriptionPreferenceRepository) {
    super();
    this.subscriptionPreferenceRepository = subscriptionPreferenceRepository;
  }

  /**
   * Method to process the incoming feed schedule request
   * @param input object containing feed schedule request
   * @return returns string post processing preference
   */
  @Override
  public String process(ReportingScheduleRequestInternal input) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, ReportingFeedConstants.CONST_METHOD_PROCESS,
      ReportingFeedConstants.METHOD_ENTERED);
    LOGGER.info("YL Subscription Feed Initiated");
    subscriptionFeedUtil.feedConfig.setStoreId(String.valueOf(input.getStoreId()));
    subscriptionFeedUtil.setRetryCount(input.getRequest().getParams());
    subscriptionFeedUtil.processLoginAndStore(input);
    subscriptionFeedUtil.setDemoMode(input.getRequest().getParams());
    String toRet = processSubscriptionPreferences(input);
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, ReportingFeedConstants.CONST_METHOD_PROCESS,
      ReportingFeedConstants.METHOD_EXITED);
    return toRet;
  }

  /**
   * Method to process subscription Preferences
   * @param subscriptionScheduleRequest Request object containing feed schedule inputs
   * @return returns string post processing preference
   */
  public String processSubscriptionPreferences(ReportingScheduleRequestInternal subscriptionScheduleRequest) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "processSubscriptionPreferences",
      ReportingFeedConstants.METHOD_ENTERED);
    String toRet = "";
    List<CustomProperty> customParams = subscriptionScheduleRequest.getRequest().getParams();
    final QReportingPreferenceEntity root = QReportingPreferenceEntity.subscriptionpreference;
    int currentDate = getCurrentDate(customParams);
    LOGGER.info("Subscription Feed processing date: {}", currentDate);
    LOGGER.info("Subscription Collection Id: {}", subscriptionFeedUtil.feedConfig.getSubscriptionCollectionId());
    BooleanExpression compositePredicate = root.collectionId
      .eq(subscriptionFeedUtil.feedConfig.getSubscriptionCollectionId());
    compositePredicate = compositePredicate.and(root.orderProcessingDate.eq(currentDate));
    int limit = ReportingFeedConstants.DEFAULT_LIMIT;
    String requestLimit = subscriptionFeedUtil.getCustomParam(customParams, ReportingFeedConstants.PARAM_LIMIT);
    if (requestLimit != null) {
      limit = Integer.valueOf(requestLimit);
    }
    String userIds = subscriptionFeedUtil.getCustomParam(customParams, ReportingFeedConstants.PARAM_USER_ID);
    if (userIds != null) {
      compositePredicate = subscriptionFeedUtil.appendUserIdstoPredicate(userIds, compositePredicate);
    }
    long totalRecords = subscriptionPreferenceRepository.count(compositePredicate);
    LOGGER.info("Total Processable Records: {}", totalRecords);
    long totalPageCount = 0;
    if (totalRecords % limit == 0) {
      totalPageCount = totalRecords / limit;
    } else {
      totalPageCount = totalRecords / limit + 1;
    }

    LOGGER.info("Total Processable Pages: {}", totalPageCount);
    subscriptionFeedUtil.makeSendNotificationRequestWithoutAttachment();
    ProcessThread processThread = new ProcessThread(compositePredicate, (int) totalPageCount, limit,
      subscriptionPreferenceRepository, subscriptionFeedUtil);
    processThread.processSubscriptionPreference();
    toRet = "Feed triggered successfully";
    LOGGER.info(toRet);
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "processSubscriptionPreferences",
      ReportingFeedConstants.METHOD_ENTERED);
    return toRet;
  }

  private int getCurrentDate(List<CustomProperty> customDetails) {
    String timeZone = subscriptionFeedUtil.feedConfig.getSubscriptionTimeZone();
    LOGGER.info("Process TimeZone: {}", timeZone);
    String dateParam = subscriptionFeedUtil.getCustomParam(customDetails, ReportingFeedConstants.PARAM_CURRENT_DAY);
    int currentDate = 0;
    if (dateParam != null) {
      currentDate = Integer.parseInt(dateParam);
    } else {
      currentDate = Calendar.getInstance(TimeZone.getTimeZone(timeZone)).get(Calendar.DAY_OF_MONTH);
    }
    return currentDate;
  }
}
