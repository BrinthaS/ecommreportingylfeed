/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.curator.shaded.com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Predicates;
import com.skava.core.configuration.SwaggerGlobalParameters;
import com.skava.core.model.SwaggerGlobalParameter;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;

import lombok.NoArgsConstructor;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * This is the SubscriptionApplication class. Starting point of the subscription
 * application.
 * 
 * @author Skava Team
 *
 */
@EnableSwagger2
@SpringBootApplication(scanBasePackages = "com.skava")
@EnableCaching
@EnableJpaRepositories
@Configuration
@NoArgsConstructor
public class ReportingFeedApplication {

  @Autowired
  private SwaggerGlobalParameters globalParamProps;

  /** The Subscription feed application logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(ReportingFeedApplication.class);

  /*
   * Api description
   */
  private static final String API_DESCRIPTION = "<div><h3>SUBSCRIPTION FEED</h3></div>"
    + ReportingFeedConstants.SUBSCRIPTION_INTRO;

  protected static final List<ResponseMessage> postMethod = Lists
    .newArrayList(
      new ResponseMessageBuilder().code(ReportingFeedConstants.RESPONSE_CODE_201)
        .message(ReportingFeedConstants.RESPONSE_MESSAGE_201).build(),
      new ResponseMessageBuilder().code(ReportingFeedConstants.RESPONSE_CODE_401)
        .message(ReportingFeedConstants.RESPONSE_MESSAGE_401).build(),
      new ResponseMessageBuilder().code(ReportingFeedConstants.RESPONSE_CODE_403)
        .message(ReportingFeedConstants.RESPONSE_MESSAGE_403).build(),
      new ResponseMessageBuilder().code(ReportingFeedConstants.RESPONSE_CODE_404)
        .message(ReportingFeedConstants.RESPONSE_MESSAGE_404).build(),
      new ResponseMessageBuilder().code(ReportingFeedConstants.RESPONSE_CODE_422)
        .message(ReportingFeedConstants.RESPONSE_MESSAGE_422).build(),
      new ResponseMessageBuilder().code(ReportingFeedConstants.RESPONSE_CODE_500)
        .message(ReportingFeedConstants.RESPONSE_MESSAGE_500).build());

  /**
   * <h1>main</h1>
   * <p>
   * This method is the main method,which gets the input as args to spring boot
   * SubscriptionFeedApplication.
   * </p>
   *
   * @param args This optional parameter contains the spring profile properties
   *             details for spring boot SubscriptionFeedApplication.
   */
  public static void main(String[] args) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "main", ReportingFeedConstants.METHOD_ENTERED);
    SpringApplication.run(ReportingFeedApplication.class, args);
    LOGGER.info("{} microservice started", "subscription feed");
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "main", ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * <h1>api</h1>
   * <p>
   * This method is used to configure the Swagger api documentation for
   * subscription application.
   * </p>
   * 
   * @return it returns the swagger documentation for subscription application
   */
  @Bean
  public Docket api() {
    List<Parameter> parameterBuilders = new ArrayList<>();
    if (globalParamProps.isEnabled()) {
      parameterBuilders.addAll(globalQueryParams());
    }

    Set<String> contentType = new HashSet<>();
    contentType.add("application/json");
    return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.skava."))
      .paths(Predicates.not(PathSelectors.regex(".*\\/getTestTokens"))).build().produces(contentType)
      .apiInfo(apiInfo()).useDefaultResponseMessages(false)
      .globalResponseMessage(RequestMethod.POST, postMethod)
      .globalOperationParameters(parameterBuilders).apiInfo(apiInfo());
  }

  /**
  * Method to set global query parameters to the Docket object. This setting will
  * be effective for all API end points within the Micro service.
  * 
  * @return
  */
  private List<Parameter> globalQueryParams() {
    List<Parameter> parameterBuilders = new ArrayList<>();

    Iterator<SwaggerGlobalParameter> iterator = null;
    SwaggerGlobalParameter globalParameter = null;

    if (globalParamProps != null && !globalParamProps.getParameters().isEmpty()) {

      iterator = globalParamProps.getParameters().iterator();

      while (iterator.hasNext()) {
        globalParameter = iterator.next();

        parameterBuilders.add(new ParameterBuilder().name(globalParameter.getParameterName())
          .description(globalParameter.getDescription())
          .modelRef(new ModelRef(globalParameter.getDataType()))
          .parameterType(globalParameter.getParameterType()).required(globalParameter.isRequired())
          .build());

      }
    }

    return parameterBuilders;
  }

  /**
   * <h1>apiInfo</h1>
   * <p>
   * This method is used to configure the api information for Swagger.
   * </p>
   * 
   * @return It returns api information in swagger.
   */
  private static ApiInfo apiInfo() {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "apiInfo", ReportingFeedConstants.METHOD_ENTERED);
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "apiInfo", ReportingFeedConstants.METHOD_EXITED);
    return new ApiInfo("Subscription Feed Services", API_DESCRIPTION,
      ReportingFeedConstants.CONST_DEFAULT_API_VERSION_ID,
      null, null, null, null, Collections.emptyList());
  }
}
