/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.skava.core.ECommerceException;
import com.skava.core.validation.ValidateException;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;
import com.skava.yl.feed.report.model.CustomProperty;
import com.skava.yl.feed.report.model.ReportingFeedConfigModel;
import com.skava.yl.feed.report.model.entity.QReportingPreferenceEntity;
import com.skava.yl.feed.report.model.request.ReportingScheduleRequestInternal;
import com.skava.yl.feed.report.util.ExcelUtil;

import lombok.Getter;
import lombok.Setter;

/**
 * Utility class for subscription feed.
 * 
 * @author lakshmi.s41
 *
 */
@Getter
@Setter
public class ReportingFeedUtil {

  public ReportingFeedConfigModel feedConfig = new ReportingFeedConfigModel();

  public List<Long> successList = Collections.synchronizedList(new ArrayList<>());
  public List<Long> failureList = Collections.synchronizedList(new ArrayList<>());
  public ObjectMapper objectMapper = new ObjectMapper();

  private static final Logger LOGGER = LoggerFactory.getLogger(ReportingFeedUtil.class);

  public ReportingFeedUtil() {
    super();
  }

  /**
   * Gets the default header info
   * 
   * @param collectionId - identifier of the collection to which preference belongs
   * @return- returns the map of headers
   */
  public Map<String, String> getDefaultHeaders(String collectionId) {
    Map<String, String> headersMap = new HashMap<>();

    headersMap.put(ReportingFeedConstants.PARAM_X_COLLECTION_ID, collectionId);
    headersMap.put(ReportingFeedConstants.PARAM_X_API_KEY, ReportingFeedConstants.PARAM_X_API_KEY_VALUE);

    return headersMap;
  }

  /**
   * @param userId
   * @param compositePredicate
   * @return
   */
  public BooleanExpression appendUserIdstoPredicate(String userId, BooleanExpression compositePredicate) {
    String[] userIds = null;
    List<Long> userIdValues = new ArrayList<>();
    if (userId != null && !userId.isEmpty()) {
      userIds = userId.split(",");

      for (String id : userIds) {
        userIdValues.add(Long.parseLong(id));
      }
      QReportingPreferenceEntity root = QReportingPreferenceEntity.subscriptionpreference;
      compositePredicate = compositePredicate.and(root.userId.in(userIdValues));

    }
    return compositePredicate;
  }

  /**
   * Method to process Login credentials and store details
   * 
   * @param input - feed schedule request
   */
  public void processLoginAndStore(ReportingScheduleRequestInternal input) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "processLoginAndStore",
      ReportingFeedConstants.METHOD_ENTERED);
    List<CustomProperty> params = input.getRequest().getParams();

    validateAndSetServiceConfig(getCustomParam(params, ReportingFeedConstants.SERVICE_CONFIG));
    setAuthData(getCustomParam(params, ReportingFeedConstants.AUTH_CONFIG));
    setNotificationConfig(getCustomParam(params, ReportingFeedConstants.NOTFICATION_SERVICE_CONFIG));
    setThreadLimitFromConfig(params);
    if (isGetStoreSuccess(feedConfig.getStoreId())) {
      processLoginAndSession();
    } else {
      throw new ValidateException("STRE001", "Get Store Failure");
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "processLoginAndStore",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * Method to set the retry count upon feed trigger failure
   * 
   * @param params - parameters arriving as part of feed schedule request
   */
  public void setRetryCount(List<CustomProperty> params) {
    String retryCount = getCustomParam(params, ReportingFeedConstants.RETRY_COUNT_VALUE);
    if (retryCount != null) {
      feedConfig.setRetryCount(Long.valueOf(retryCount));
    } else {
      feedConfig.setRetryCount(ReportingFeedConstants.DEFAULT_RETRY_COUNT);
    }
  }

  /**
   * Method to set the demo mode
   * 
   * @param params - parameters arriving as part of feed schedule request
   */
  public void setDemoMode(List<CustomProperty> params) {
    String enableDeomoMode = getCustomParam(params, ReportingFeedConstants.ENABLE_DEMO_MODE_VALUE);
    if (enableDeomoMode != null) {
      feedConfig.setEnableDemoMode(enableDeomoMode);
    }
  }

  /**
   * Method to process Login info and session details
   */
  public void processLoginAndSession() {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "processLoginAndSession",
      ReportingFeedConstants.METHOD_ENTERED);
    if (isLoginSuccess()) {
      getSession(true, String.valueOf(ReportingFeedConstants.DEFAULT_AUTH_COLLECTION_ID));
    } else {
      throw new ValidateException("AUTH001", "User login Failure");
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "processLoginAndSession",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * Method to to validate if login succeeded or failed
   * 
   * @return returns true or false 
   */
  public boolean isLoginSuccess() {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "isLoginSuccess",
      ReportingFeedConstants.METHOD_ENTERED);
    boolean toRet = false;
    String url = Utils.getUrl(ReportingFeedConstants.SERVICE_CONFIG_CUSTOMER_CONSTANT,
      feedConfig.getServiceConfig()) + ReportingFeedConstants.USER_AUTH_URL;

    JSONObject userRequest = new JSONObject();
    ResponseEntity<String> response = null;
    JSONObject responseJson = null;
    Map<String, String> headersMap = getDefaultHeaders(ReportingFeedConstants.DEFAULT_AUTH_COLLECTION_ID);
    userRequest.put(ReportingFeedConstants.PARAM_USER_IDENTITY, feedConfig.getSuperAdminUser());
    userRequest.put("password", feedConfig.getSuperAdminPwd());

    try {
      Object obj = objectMapper.readValue(userRequest.toString(), Object.class);
      response = Utils.makeAPIRequest(obj, url, String.class, headersMap, HttpMethod.POST);

      if (response != null) {
        responseJson = new JSONObject(response.getBody());
        if ((response.getStatusCode() == HttpStatus.ACCEPTED)) {
          toRet = true;
          feedConfig.setBearerToken(responseJson.getString("bearerToken"));
          responseJson = (JSONObject) responseJson.get("users");
          String userId = String.valueOf(responseJson.get("id"));
          feedConfig.setSuperAdminUserId(userId);
          LOGGER.info("Login API Success for UserId: {}", userId);
        } else {
          LOGGER.error("Login Failure in User Auth : {}", responseJson);
        }
      }

    } catch (IOException e) {
      LOGGER.error("Login Failure in User Auth : {0}", e);
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "isLoginSuccess",
      ReportingFeedConstants.METHOD_EXITED);
    return toRet;
  }

  /**
   * Method to know if store details retrieved successfully
   * 
   * @param storeId identifier of the store whose details is to be retrieved
   * @return returns true or false
   */
  public boolean isGetStoreSuccess(String storeId) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "isGetStoreSuccess",
      ReportingFeedConstants.METHOD_ENTERED);
    boolean toRet = false;
    String url = Utils.getUrl(ReportingFeedConstants.SERVICE_CONFIG_FOUNDATION_CONSTANT,
      feedConfig.getServiceConfig()) + ReportingFeedConstants.STORE_URL + storeId;
    ResponseEntity<String> response = null;
    JSONObject responseJson = null;

    response = Utils.makeAPIRequest(null, url, String.class, null, HttpMethod.GET);
    if (response != null) {
      responseJson = new JSONObject(response.getBody());
      if ((response.getStatusCode() == HttpStatus.OK)) {
        LOGGER.info("Get Store API success for StoreId: {}", storeId);
        toRet = true;
        parseStoreResponse(responseJson);
      } else {
        LOGGER.error("Get Store Failure in Foundation : {}", responseJson);
      }
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "isGetStoreSuccess",
      ReportingFeedConstants.METHOD_EXITED);
    return toRet;
  }

  /**
   * Method to parse the store response
   * 
   * @param responseJson json object containing retrieved store information
   */
  private void parseStoreResponse(JSONObject responseJson) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "parseStoreResponse",
      ReportingFeedConstants.METHOD_ENTERED);
    JSONArray associations = responseJson.optJSONArray(ReportingFeedConstants.FIELD_ASSOCIATIONS);
    if (associations != null && associations.length() > 0) {
      for (Object object : associations) {
        JSONObject association = (JSONObject) object;
        String timeZone = responseJson.getString(ReportingFeedConstants.FIELD_TIME_ZONE);
        getCollectionIdForService(association, timeZone);
      }
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "parseStoreResponse",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * Method to get the collection Id
   * @param association json object containing association info
   * @param timeZone Timezone in which processing is done
   */
  private void getCollectionIdForService(JSONObject association, String timeZone) {
    String serviceName = association.optString(ReportingFeedConstants.FIELD_NAME);
    long collectionID = association.optLong(ReportingFeedConstants.FIELD_COLLECTIONID);
    if (collectionID > 0) {
      if (ReportingFeedConstants.SERVICE_CONFIG_AUTH_CONSTANT.equals(serviceName)) {
        feedConfig.setAuthCollectionId(collectionID);
      } else if (ReportingFeedConstants.SERVICE_CONFIG_SUBSCRIPTION_CONSTANT.equals(serviceName)) {
        feedConfig.setSubscriptionCollectionId(collectionID);
        feedConfig.setSubscriptionTimeZone(timeZone);
      } else if (ReportingFeedConstants.SERVICE_CONFIG_NOTIFY_CONSTANT.equals(serviceName)) {
        feedConfig.setNotificationCollectionId(collectionID);
      }
    }
  }

  /**
   * Method to validate service configuration
   * 
   * @param serviceConfigString String of service configuration info
   */
  private void validateAndSetServiceConfig(String serviceConfigString) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "validateAndSetServiceConfig",
      ReportingFeedConstants.METHOD_ENTERED);
    JSONObject serviceConfigObj = null;
    if (serviceConfigString != null && !serviceConfigString.isEmpty() && serviceConfigString.startsWith("{")
      && serviceConfigString.endsWith("}")) {
      serviceConfigObj = new JSONObject(serviceConfigString);
      String[] serviceNames = {ReportingFeedConstants.SERVICE_CONFIG_ORCH_CONSTANT,
        ReportingFeedConstants.SERVICE_CONFIG_AUTH_CONSTANT,
        ReportingFeedConstants.SERVICE_CONFIG_FOUNDATION_CONSTANT,
        ReportingFeedConstants.SERVICE_CONFIG_SUBSCRIPTION_CONSTANT,
        ReportingFeedConstants.SERVICE_CONFIG_CUSTOMER_CONSTANT};
      Set<String> serviceNamesKey = serviceConfigObj.keySet();
      for (int idx = 0; idx < serviceNames.length; idx++) {
        if (!serviceNamesKey.contains(serviceNames[idx])) {
          throw new ValidateException("SUB001", serviceNames[idx] + "ServiceConfig missing");
        }
      }
    } else {
      throw new ValidateException("SUB002", "Validate the given serviceConfig");
    }
    feedConfig.setServiceConfig(serviceConfigObj);
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "validateAndSetServiceConfig",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * Method to set authorization data in config
   * 
   * @param authConfig String of authorization config info
   */
  public void setAuthData(String authConfig) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "setAuthData",
      ReportingFeedConstants.METHOD_ENTERED);
    if (authConfig != null && !authConfig.isEmpty()) {
      JSONObject authConfigObj = new JSONObject(authConfig);
      feedConfig.setSuperAdminUser(authConfigObj.optString(ReportingFeedConstants.AUTH_CONFIG_USER_NAME));
      feedConfig.setSuperAdminPwd(authConfigObj.optString("credentials"));
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "setAuthData",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * Method to set notification configuration
   * 
   * @param notificationConfig String of notification config info
   */
  public void setNotificationConfig(String notificationConfig) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "setNotificationConfig",
      ReportingFeedConstants.METHOD_ENTERED);
    if (notificationConfig != null && !notificationConfig.isEmpty()) {
      JSONObject notificationConfigObj = new JSONObject(notificationConfig);
      feedConfig.setNotificationConfig(notificationConfigObj);
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "setNotificationConfig",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * Method to construct URL for processNow API
   * 
   * @param userIdValue Identifier of the subscribed user
   * @return returns well formed URL
   */
  public String processNowAPIURl(long userIdValue) {
    return Utils.getUrl(ReportingFeedConstants.SERVICE_CONFIG_ORCH_CONSTANT, feedConfig.getServiceConfig())
      + ReportingFeedConstants.SUBSCRIPTION_PREFERENCE_SUFFIX_URL + userIdValue
      + ReportingFeedConstants.PARAM_QUESTION + ReportingFeedConstants.PARAM_STORE_ID
      + ReportingFeedConstants.PARAM_EQUAL + feedConfig.getStoreId() + ReportingFeedConstants.PARAM_AMP
      + ReportingFeedConstants.APPLY_STORE_CREDIT + ReportingFeedConstants.PARAM_EQUAL + "false";
  }

  /**
   * Method to construct notification URL
   * 
   * @return returns well formed URL
   */
  public String notificationUrl() {
    return Utils.getUrl(ReportingFeedConstants.SERVICE_CONFIG_NOTIFY_CONSTANT, feedConfig.getServiceConfig())
      + ReportingFeedConstants.NOTIFICATION_URL;
  }

  /**
   * Method to get session detailsget
   * 
   * @param isSuperAdminLogin true if superadmin, false otherwise
   * @param userIdValue identifier of the subscribed user
   * @return returns string of session token
   */
  public String getSession(boolean isSuperAdminLogin, String userIdValue) {

    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "getSession",
      ReportingFeedConstants.METHOD_ENTERED);
    String sessionId = null;
    String url = Utils.getUrl(ReportingFeedConstants.SERVICE_CONFIG_AUTH_CONSTANT, feedConfig.getServiceConfig())
      + ReportingFeedConstants.AUTH_URL + userIdValue + "&bearerToken=" + feedConfig.getBearerToken();
    ResponseEntity<String> response = null;

    String collectionId = String.valueOf(feedConfig.getAuthCollectionId());
    if (isSuperAdminLogin) {
      collectionId = String.valueOf(ReportingFeedConstants.DEFAULT_AUTH_COLLECTION_ID);
    }
    Map<String, String> headersMap = getDefaultHeaders(collectionId);
    if (feedConfig.getAuthToken() != null) {
      headersMap.put(ReportingFeedConstants.PARAM_X_AUTH_TOKEN, feedConfig.getAuthToken());
    }
    if (userIdValue != null) {
      response = Utils.makeAPIRequest(null, url, String.class, headersMap, HttpMethod.POST);
      if (response != null) {
        sessionId = setAuthToken(response, isSuperAdminLogin, userIdValue);
      }
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "getSession", ReportingFeedConstants.METHOD_EXITED);
    return sessionId;
  }

  private String setAuthToken(ResponseEntity<String> response, boolean isSuperAdminLogin, String userIdValue) {
    String sessionId = null;
    JSONObject responseJson = new JSONObject(response.getBody());
    if ((response.getStatusCode() == HttpStatus.CREATED)) {
      sessionId = responseJson.optString("sessionId");
      if (isSuperAdminLogin) {
        feedConfig.setAuthToken(responseJson.optString(ReportingFeedConstants.PARAM_AUTH_TOKEN));
        LOGGER.info("Get Session API success for UserId: {}", userIdValue);
      }

    } else {
      LOGGER.error("Get Session Failure in Auth : {}", responseJson);
    }
    return sessionId;
  }

  /**
   * Method to get Service configuration data
   * 
   * @param params parameters to configure service
   * @param propertyName properties within config 
   * @return returns well formed service config
   */
  public String getCustomParam(List<CustomProperty> params, String propertyName) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "getServiceConfig",
      ReportingFeedConstants.METHOD_ENTERED);
    String serviceConfig = null;
    if (params != null) {
      for (CustomProperty property : params) {
        if (property.getName().equals(propertyName)) {
          serviceConfig = property.getStringValue();
        }
      }
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "getServiceConfig",
      ReportingFeedConstants.METHOD_EXITED);
    return serviceConfig;
  }

  /**
   * Method to send notifications
   * 
   */
  public void sendNotification() {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "sendNotification",
      ReportingFeedConstants.METHOD_ENTERED);
    HashMap<String, List<Long>> notificationList = new HashMap<>();
    notificationList.put("Success", successList);
    notificationList.put("Failure", failureList);

    String directory = getConfigValues(feedConfig.getNotificationConfig(), "destinationFolder");

    File excelFile = ExcelUtil.writeExcel(directory, ReportingFeedConstants.NOTIFICATION_FILE_NAME,
      notificationList);
    try (FileInputStream excelStreamData = new FileInputStream(excelFile)) {
      byte[] fileInBytes = new byte[(int) excelFile.length()];
      excelStreamData.read(fileInBytes, 0, fileInBytes.length);
      String fileContent = Base64.encodeBase64String(fileInBytes);
      makeSendNotificationRequest(fileContent);
    } catch (IOException e) {
      LOGGER.error("Exception while reading excel file content {}", e);
      throw new ECommerceException(ReportingFeedConstants.RESPONSE_MESSAGE_500,
        "Exception while reading excel file content");
    }
    LOGGER.info("Mail successfully triggered");
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "sendNotification",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * Method to make notification request
   * 
   * @param fileContent content within the file 
   * 
   */
  public void makeSendNotificationRequest(String fileContent) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "makeSendNotificationRequest",
      ReportingFeedConstants.METHOD_ENTERED);
    try {
      HashMap<String, String> headerMap = new HashMap<>();
      headerMap.put(ReportingFeedConstants.PARAM_X_COLLECTION_ID,
        String.valueOf(feedConfig.getNotificationCollectionId()));
      headerMap.put(ReportingFeedConstants.PARAM_X_API_KEY, ReportingFeedConstants.PARAM_X_API_KEY_VALUE);
      JSONObject notificationConfigValue = feedConfig.getNotificationConfig();
      String recipientEmail = notificationConfigValue.optString(ReportingFeedConstants.FIELD_RECIPENTS_EMAIL);
      String templateName = notificationConfigValue.optString(ReportingFeedConstants.FIELD_TEMPLATE_NAME);
      String attachmentFileName = notificationConfigValue
        .optString(ReportingFeedConstants.FIELD_ATTACHMENTS_NAME);

      JSONObject notificationRequest = new JSONObject();
      JSONArray recipients = new JSONArray();
      JSONArray attachments = new JSONArray();
      JSONObject macros = new JSONObject();

      JSONObject attachment = new JSONObject();
      attachment.put(ReportingFeedConstants.NOTIFICATION_REQUEST_ATTACHEMENT_FILE_NAME, attachmentFileName);
      attachment.put(ReportingFeedConstants.NOTIFICATION_REQUEST_ATTACHEMENT_FILE_CONTENT, fileContent);
      attachment.put(ReportingFeedConstants.NOTIFICATION_REQUEST_ATTACHEMENT_CONTENT_SOURCE, "content");
      attachments.put(0, attachment);

      JSONObject recipientRequest = new JSONObject();
      recipientRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_EMAIL, recipientEmail);
      recipientRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_ATTACHEMENTS, attachments);
      recipientRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_MACROS, macros);
      recipients.put(0, recipientRequest);

      notificationRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_RECIPIENTS, recipients);
      notificationRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_NAME, templateName);

      Object obj = objectMapper.readValue(notificationRequest.toString(), Object.class);
      ResponseEntity<String> notificationAPIResponse = Utils.makeAPIRequest(obj, notificationUrl(), String.class,
        headerMap, HttpMethod.POST);
      LOGGER.debug(notificationAPIResponse.getBody());
    } catch (HttpStatusCodeException e) {
      LOGGER.error("Error in send the Notification mail threw request {}", e);
    } catch (IOException e) {
      LOGGER.error("Error in send the Notification mail threw request {}", e);
      throw new ECommerceException(ReportingFeedConstants.RESPONSE_MESSAGE_500,
        "Exception while sending notification with attachment");
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "makeSendNotificationRequest",
      ReportingFeedConstants.METHOD_EXITED);

  }

  /**
   * Method to send notification request without any attachment contained in it
   * 
   * 
   */
  public void makeSendNotificationRequestWithoutAttachment() {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "makeSendNotificationRequestWithoutAttachment",
      ReportingFeedConstants.METHOD_ENTERED);
    try {
      HashMap<String, String> headerMap = new HashMap<>();
      headerMap.put(ReportingFeedConstants.PARAM_X_COLLECTION_ID,
        String.valueOf(feedConfig.getNotificationCollectionId()));
      headerMap.put(ReportingFeedConstants.PARAM_X_API_KEY, ReportingFeedConstants.PARAM_X_API_KEY_VALUE);
      JSONObject notificationConfigValue = feedConfig.getNotificationConfig();
      String recipientEmail = notificationConfigValue.optString(ReportingFeedConstants.FIELD_RECIPENTS_EMAIL);
      String templateName = notificationConfigValue.optString(ReportingFeedConstants.FIELD_TEMPLATE_NAME);

      JSONObject notificationRequest = new JSONObject();
      JSONArray recipients = new JSONArray();
      JSONArray attachments = new JSONArray();
      JSONObject macros = new JSONObject();
      JSONObject recipientRequest = new JSONObject();
      recipientRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_EMAIL, recipientEmail);
      recipientRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_ATTACHEMENTS, attachments);
      recipientRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_MACROS, macros);
      recipients.put(0, recipientRequest);

      notificationRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_RECIPIENTS, recipients);
      notificationRequest.put(ReportingFeedConstants.NOTIFICATION_REQUEST_NAME, templateName);

      Object obj = objectMapper.readValue(notificationRequest.toString(), Object.class);
      ResponseEntity<String> notificationAPIResponse = Utils.makeAPIRequest(obj, notificationUrl(), String.class,
        headerMap, HttpMethod.POST);
      LOGGER.debug(notificationAPIResponse.getBody());
    } catch (HttpStatusCodeException e) {
      LOGGER.error("Exception while sending notification without attachments {}", e);
      throw new ECommerceException(ReportingFeedConstants.RESPONSE_MESSAGE_500,
        "Exception while sending notification without attachments");
    } catch (IOException e) {
      LOGGER.error("Error in send the Notification mail without Attachmeent data {}", e);
      throw new ECommerceException(ReportingFeedConstants.RESPONSE_MESSAGE_500,
        "Exception while sending notification without attachments");
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "makeSendNotificationRequestWithoutAttachment",
      ReportingFeedConstants.METHOD_EXITED);

  }

  /**
   * Method to config details from json object property
   * 
   * @param configDetails json object containing configuration details
   * @param propertyName name of the properties in config
   * @return returns string of config values
   */
  public String getConfigValues(JSONObject configDetails, String propertyName) {
    String propValue = null;
    if (configDetails != null && configDetails.has(propertyName)) {
      propValue = configDetails.getString(propertyName);
    }
    return propValue;
  }

  /**
   * Method to set Thread limit from config
   * 
   * @param params parameters requires for thread configuration
   */
  public void setThreadLimitFromConfig(List<CustomProperty> params) {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "setThreadLimitFromConfig",
      ReportingFeedConstants.METHOD_ENTERED);
    String threadLimit = getCustomParam(params, ReportingFeedConstants.THREAD_CONFIG);
    if (threadLimit != null && threadLimit.length() > 0) {
      feedConfig.setThreadLimit(threadLimit);
    } else {
      feedConfig.setThreadLimit("30");
    }
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "setThreadLimitFromConfig",
      ReportingFeedConstants.METHOD_EXITED);
  }

}
