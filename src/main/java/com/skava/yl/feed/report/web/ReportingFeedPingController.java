package com.skava.yl.feed.report.web;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;

/**
 * The Class SubscriptionFeedPingController
 * 
 * @author aravind.e
 */
@SwaggerDefinition(
  info = @Info(title = "EcommSubscriptionFeed", version = "v8.0", description = "Using EcommSubscriptionFeed"))
@Api(value = "Ping", tags = "Ping")
@RestController
public class ReportingFeedPingController {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReportingFeedPingController.class);

  @ApiOperation(value = "Ping controller", response = String.class, nickname = "ping")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "success", response = String.class),
    @ApiResponse(code = 404, message = "the resource you are trying to reach is not found")})
  /**
   * Responses ping call for SubscriptionFeed. Ability to check if the server is
   * reachable.
   * 
   * @return current time is returned in milliSeconds
   */
  @GetMapping(value = "/ping")
  public String ping() {
    LOGGER.debug("### {} {} ###", "ping", "Entered");
    // local date time
    Date date = new Date();
    LOGGER.debug("### {} {} ###", "ping", "Exit");
    return String.format("subscription feed is pinging : %s", date.toString());
  }
}
