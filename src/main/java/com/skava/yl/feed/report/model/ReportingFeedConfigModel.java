/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.model;

import org.json.JSONObject;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This is a model class for subscription feed Config details
 * @author brintha.s01
 *
 */
@Getter
@Setter
@NoArgsConstructor
public class ReportingFeedConfigModel {
  private JSONObject serviceConfig;
  private JSONObject notificationConfig;
  private String threadLimit;
  private String bearerToken;
  private String superAdminUserId;
  private String userId;
  private String storeId;
  private long subscriptionCollectionId;
  private long notificationCollectionId;
  private String subscriptionTimeZone;
  private long authCollectionId;
  private String authToken;
  private String superAdminUser;
  private String superAdminPwd;
  private long retryCount;
  private String enableDemoMode;
}
