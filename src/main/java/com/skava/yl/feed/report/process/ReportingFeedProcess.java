/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.process;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ForkJoinPool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import com.querydsl.core.types.Predicate;
import com.skava.yl.feed.report.constant.ReportingFeedConstants;
import com.skava.yl.feed.report.helper.ReportingFeedUtil;
import com.skava.yl.feed.report.helper.Utils;
import com.skava.yl.feed.report.model.entity.ReportingPreferenceEntity;
import com.skava.yl.feed.report.repository.ReportingPreferenceRepository;

/**
 * 
 * @author lakshmi.s41
 *
 */
public class ReportingFeedProcess {

  /**
   * Logger is used to print the Logger details to the specific file.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(ReportingFeedProcess.class);

  /**
   * used to construct the query using predicate.
   */
  private Predicate predicate;

  /**
   * Holds the pagable data with different offset and limit.
   */
  private Pageable pageable;

  /**
   * Holds the methods of repository class
   */
  private ReportingPreferenceRepository subscriptionPreferenceRepository;

  /**
   * object used to access the subscriptionFeedUtill methods and variables.
   */
  private ReportingFeedUtil subscriptionFeedUtil;

  public ReportingFeedProcess(Predicate predicate, Pageable pageable,
    ReportingPreferenceRepository subscriptionPreferenceRepository,
    ReportingFeedUtil subscriptionFeedUtil) {
    this.predicate = predicate;
    this.pageable = pageable;
    this.subscriptionPreferenceRepository = subscriptionPreferenceRepository;
    this.subscriptionFeedUtil = subscriptionFeedUtil;
  }

  /**
   * 
   * Method to find all the matching subscription preference entities from repository
   * 
   */
  public void processSubscriptionPreference() {
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "processSubscriptionPreference",
      ReportingFeedConstants.METHOD_ENTERED);
    Long startTime = System.currentTimeMillis();
    String threadLimit = subscriptionFeedUtil.feedConfig.getThreadLimit();
    ForkJoinPool fjp = new ForkJoinPool(Integer.valueOf(threadLimit));
    LOGGER.info("Processing Page: {} ", pageable.getPageSize());
    LOGGER.info("Offset: {}", pageable.getOffset());
    LOGGER.info("Start Time(in MilliSeconds): {}", startTime);
    Page<ReportingPreferenceEntity> preferences = subscriptionPreferenceRepository.findAll(predicate, pageable);
    if (preferences != null && preferences.hasContent()) {
      List<ReportingPreferenceEntity> preferenceList = preferences.getContent();
      try {
        fjp.submit(() -> preferenceList.parallelStream().forEach(preference -> processPreferance(preference))).get();
      } catch (Exception e) {
        LOGGER.error("Exception ThreadName : {}", Thread.currentThread().getName());
        LOGGER.error("Exception in processing subscription preference : {}", e);
      }

    }
    Long endTime = System.currentTimeMillis() - startTime;
    LOGGER.info("Processing ended for Page size: {} ", pageable.getPageSize());
    LOGGER.info("Offset: {}", pageable.getOffset());
    LOGGER.info("TimeTaken(in MilliSeconds): {}", endTime);
    LOGGER.debug(ReportingFeedConstants.DEBUG_MESSAGE_FORMAT, "processSubscriptionPreferences",
      ReportingFeedConstants.METHOD_EXITED);
  }

  /**
   * 
   * Method to process the subscription preference entity
   * @param preference
   * 
   */
  private void processPreferance(ReportingPreferenceEntity preference) {
    long currentUserId = preference.getUserId();
    long retryCount = subscriptionFeedUtil.feedConfig.getRetryCount();
    LOGGER.info("Started Processing for UserId: {}", currentUserId);
    String session = subscriptionFeedUtil.getSession(false, String.valueOf(currentUserId));
    HashMap<String, String> headersMap = new HashMap<>();
    headersMap.put(ReportingFeedConstants.PARAM_X_SESSION_ID, session);
    String demoMode = subscriptionFeedUtil.feedConfig.getEnableDemoMode();
    if (demoMode != null) {
      headersMap.put(ReportingFeedConstants.HEADER_IS_USER_SUBSCRIPTION_PREFERENCE_DEMO, demoMode);
    }
    do {
      retryCount = makeProcessNowRequest(currentUserId, retryCount, headersMap);
    } while (retryCount != 0);
  }

  /**
   * 
   * Method to construct processNow API request
   * @param currentUserId
   * @param retryCount
   * @param headersMap
   * @return retrycount value
   * 
   */
  private long makeProcessNowRequest(long currentUserId, long retryCount, HashMap<String, String> headersMap) {
    String processNowurl = subscriptionFeedUtil.processNowAPIURl(currentUserId);
    try {
      ResponseEntity<String> processNowAPIResponse = Utils.makeAPIRequest(null, processNowurl, String.class,
        headersMap, HttpMethod.POST);
      LOGGER.info("ProcessNow API responseCode: {} ", processNowAPIResponse.getStatusCode());
      LOGGER.info("ProcessNow API ResponseMsg: {}", processNowAPIResponse.getBody());
      LOGGER.info("ProcessNow API for UserId: {}", currentUserId);
      subscriptionFeedUtil.successList.add(currentUserId);
      retryCount = 0;
    } catch (HttpServerErrorException e) {
      LOGGER.debug("Exeception in processing request {}", e);
      LOGGER.error("Exeception in processing request {}", e.getResponseBodyAsString());
      if (org.springframework.http.HttpStatus.BAD_GATEWAY.value() == e.getRawStatusCode()) {
        retryCount--;
        subscriptionFeedUtil.processLoginAndSession();
      } else {
        retryCount = 0;
      }
    } catch (HttpClientErrorException e) {
      LOGGER.debug("Exception ThreadName : {}", Thread.currentThread().getName());
      LOGGER.debug("Client Error Exception {}", e);
      LOGGER.error("Client Error Exception {}", e.getResponseBodyAsString());
      subscriptionFeedUtil.failureList.add(currentUserId);
      if (org.springframework.http.HttpStatus.FORBIDDEN.value() == e.getRawStatusCode()) {
        retryCount--;
        subscriptionFeedUtil.processLoginAndSession();
      } else {
        retryCount = 0;
      }
    }
    return retryCount;
  }

}
