/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.yl.feed.report.model.responce;

import com.skava.core.ResponseModel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
* 
* The Class SubscriptionFeedServiceErrorResponse
* @author Skava
*
*/

@NoArgsConstructor
@Getter
@Setter
@Accessors(chain = true)
public class ReportingFeedServiceErrorResponse extends ResponseModel {

  /**
   * serialVersionUID
   */
  /* serialVersionUID. */
  private static final long serialVersionUID = -1141206943615789105L;

  /* The status parameter contains the subscription is active or inactive. */
  @ApiModelProperty(
    value = "${SubscriptionError.status.ApiModelProperty.value}",
    required = false, readOnly = true)
  private int status;

  /**
   * 
   * @param code response code for the ServiceError
   * @param message Response message for the subscription feed service
   */
  public ReportingFeedServiceErrorResponse(String code, String message) {
    this.responseCode = code;
    this.responseMessage = message;
  }

}
