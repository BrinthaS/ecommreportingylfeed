/*******************************************************************************
 * Copyright ©2002-2018 Skava - All rights reserved.
 * 
 * All information contained herein is, and remains the property of Skava.
 * Skava including, without limitation, all software and other elements thereof, 
 * are owned or controlled exclusively by Skava and protected by copyright, patent
 * and other laws. Use without permission is prohibited. 
 * Unauthorized copying of this file, via any medium is strictly prohibited
 *    
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
/**
 *This class contains asynchronous thread configurations
 */
package com.skava.yl.feed.report.service.impl;

import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * This class is to configure asynchronous threads
 * @author lakshmi.s41
 *
 */
@Configuration
@EnableAsync
public class AsyncConfiguration {
  private static final Logger LOGGER = LoggerFactory.getLogger(AsyncConfiguration.class);
  private static final int ONE = 1;
  private static final int QCAPACITY = 5;

  /**
   * Method to configure the ThreadPoolTaskExecutor
   * @return returns the executor object
   */
  @Bean(name = "taskExecutor")
  public Executor taskExecutor() {
    LOGGER.debug("Creating Async Task Executor");
    final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(ONE);
    executor.setMaxPoolSize(ONE);
    executor.setQueueCapacity(QCAPACITY);
    executor.setThreadNamePrefix("SubscriptionPreferenceThread-");
    executor.initialize();
    return executor;
  }

}
